### Setup Network Shared Music Folder

### (Use At Your Own Risk)

Here is the process I used to setup my Linux systems to access my master Music Folder on my Ubuntu Server.
I found this on "Ask Ubuntu" web site and edited for my use:
...

This process work's for me at the time of this writing:
This is how I setup my network shared Music folder from the server to my "Music" folder in my /home/Music
in Linux..
...

## Please Note that you will have to use ---> What ever your Distro uses to install software <---

- These instructions work on Arch, Ubuntu, Debian, Solus, MX, and other Linux Distros I have tested...

- This process worked for me on my Linux network at boot time and is automatically loaded and logged into the network shared folder on the server for my music...
- This will not hang the booting computer if the network server shared folder is not available or offline or powered down.

## Please Note:
- This setup is "Read Only" from the server. If you want to add files to the folder from your system you will have to change the file permissions on the server from root I think... I changed my folder to 744 on the server so I could add files.
I ssh into my server and upload them that way... What ever way you use to add file will work now.

- The information is listed as is and in no way guaranteed to work on you setup or network or Linux Installation...
- Have fun and enjoy! LLAP -Bruce
